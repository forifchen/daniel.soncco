function sigmas = getsigmas(ww, plotting)

if nargin == 1, plotting = 0; end

[m,n] = size(ww);
ha2 = hamming(m*3);
ha2 = repmat(ha2, [1 n]);

aux = [flipud(ww); ww; flipud(ww)];
lo = abs(fft(aux .* ha2));
for c = 1:size(lo,2)
    lo(:,c) = fftshift(lo(:,c));
end
a = (1/3)*[1,1,1];
b = eye(m);
kr = kron(b, a);
kr(:, end+1) = kr(:,1); kr(:,1) = [];
po = kr*lo;
po(end+1, :) = po(1,:);

mean1 = mean(po,2);
x1 = log( linspace(0,1,m/2+1) )';
y1 = log(mean1(m/2+1:end));
% x2 = [x1(round(m/2*.1):round(m/2*.95)) ];
% y2 = [y1(round(m/2*.1):round(m/2*.95)) ];
x2 = x1(2:end-1); y2 = y1(2:end-1);

t1 = linspace(0,1,m/2+1);
% [f1, pol] = Ransac(x2, y2);
% t2 = polyval(pol, log(t1));

% b = robustfit([x2 x2.^2], y2);
% t2 = b(1) + b(2)*log(t1) + b(3)*log(t1).^2;

b = robustfit([x2 x2.^2 x2.^3], y2, 'cauchy', .5);
% figure; plot(x2,y2);
t2 = polyval( flipud(b), log(abs(t1)) );

% [f1,pol] = Ransac(exp(x2), y2);
% t2 = polyval(pol, t1);

t3 = log(mean1);
for i = 1 : 100, t3 = fixedLogs4(t3, 0, .02, .01); end

if plotting
    figure; hold on;
    plot(t1,t3(m/2+1:end), 'b');
    plot(t1,t2, 'r'); 
    plot(t1, log(mean1(m/2+1:end)), 'k' );
end

pos = find( abs( t3(m/2+1:end) - t2') > .1 );

s1 = 0; s2 = 0; lenmax = 0; i = 1; j = i + 1;
while i <= length(pos)
    while j <= length(pos) && pos(j) == pos(j-1) + 1
        j = j + 1;
    end
    if lenmax < j - i
        lenmax = j - i;
        s1 = i; s2 = j-1;
    end
    i = j;
    j = j + 1;
end


sigmas = t1(pos([s1 s2]));