% w: image, ro: remove outliers or not
% par
%   s1,s2 : interval of fringes's frequencies
%   al1,al2 : alpha indices of phi_alpha 
% oracle: first approximation
% res: final aproximation
function [res, oracle] = solving_with_fixedlogs(w, par, ro)

if nargin < 3, ro = 0; end
if nargin == 1
    sigmas = getsigmas(w);
    par.s1 = sigmas(1); par.s2 = sigmas(2);
    
    par.al1 = 5e-5;
    par.al2 = .005;
else
    if isfield(par, 'al1') == 0, par.al1 = 5e-5; end
    if isfield(par, 'al2') == 0, par.al2 = 5e-3; end
end


c1 = mean(w(:)); c2 = std(w(:))*8;
w = 1 + (w - c1) / c2;

[m,n] = size(w);
ha2 = hamming(m*3);
ha2 = repmat(ha2, [1 n]);

mask2 = zeros(m*3,1);
% interval omega of frequences of columns of matrix v
interval = round((1 - [par.s1 par.s2])/2*m*3 + 1);
left = interval(2); right = interval(1);

mask2(left:right) = 1; mask2(end-right+2:end-left+2) = 1;
mask2 = repmat(mask2, [1 n]);

% forcing columns of dft(u) to be away of interval of frequences of v in order to supress
% the main fringes
aux = [flipud(w); w; flipud(w)];
f1 = fftshift(fft(aux .* ha2));
u = real(ifft( fftshift(f1 .* (1-mask2)) )) ./ ha2;
u = u(m+1 : end-m,:);

oracle = c1 + (u - 1) * c2;



for i = 1 : 20
    % regularizing u verticallement
    u = fixedLogs4(u, 0, par.al1, par.al1/4 * 1.99);
    v = w ./ u - 1;
    
    % forcing frequences of columns of v to be in omega
    v2 = [flipud(v); v; flipud(v)];
    fv2 = fft(v2 .* ha2);    
    mfv2 = fv2 .* fftshift(mask2);
    v2 = real(ifft( mfv2 )) ./ ha2;
    v = v2(m+1 : end-m,:);    

    % regularizing v horizontallement
    v = fixedLogs4(v, 1, par.al2, par.al2/4 * 1.99);    
    u = w ./ (1 + v);

end

u = c1 + (u - 1) * c2;

if ro == 1
    res = remove_outliers(u);
else
    res = u;
end