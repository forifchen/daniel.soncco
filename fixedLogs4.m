function x = fixedLogs4(y, horiz, al, be);
% Mila Nikolova, July 30, 2013, nikolova@cmla.ens-cachan.fr
%
% y - input gray image, minimization of
% J(x)=sum_i psi(x[i]-y[i], al1) + be sum_{i,j} phi((u[i]-u[j]), al2)
% {i,j} - horizontal and vertical neighbors  
% psi, phi = 'logs' = abs(t)-al *log(1+abs(t)/al )
% horiz: boolean indicator of gradient either horizontal or vertical
% Initialization x=y (automatic)
% output : x - "restored" images
% x=fixedLogs4(y);

global par;

% x = y;
[m,n] = size(y);
%modify be_h, be_v
if horiz == 0
%     al1 = .007;
%     al2 = .1;
%     be = .10;
%     be = .05;
%.1 .001

    t = diff(y);
    f = t ./ (al + abs(t));              
    R = -diff( [zeros(1,n); f; zeros(1,n)] );
%     disp( num2str( [mean(y(:)) mean(abs(t(:))) mean(abs(R(:)))] ) );
else
%     al1 = .12;
%     al2 = .02;
%     be = .09;
%     be = .01;
% .02 .01

    t = diff(y')';
    f = t ./ (al + abs(t));              
    R = -diff( [zeros(1,m); f'; zeros(1,m)] )';
%         disp( num2str( [mean(abs(y(:))) mean(abs(t(:))) mean(abs(R(:)))] ) );

end    
% R = be * R;
% x = y - R*al1 ./ (1 - abs(R)); 
x = y - be*R;    
