function ro = remove_outliers(w)

[h,c] = hist(w(:), 256);
sh = h(1:end-1) + h(2:end);
dos = find(sh > 10);
cnt = sum(h(dos(end)+2:end)); 
disp( [num2str(cnt) ' outliers found']);
[sorted, pos] = sort(w(:));
[i,j] = ind2sub(size(w), pos(end-cnt+1:end));
% disp([i j]);
w(pos(end-cnt+1:end)) = sorted(end-cnt);
ro = w;